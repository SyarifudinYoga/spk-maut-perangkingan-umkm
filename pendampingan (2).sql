-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 31 Jan 2021 pada 08.35
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pendampingan`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `bobot`
--

CREATE TABLE `bobot` (
  `idBobot` int(11) NOT NULL,
  `Bobot_StatusKepimilikanTempatUMKM` double NOT NULL,
  `Bobot_Omset` double NOT NULL,
  `Bobot_Aset` double NOT NULL,
  `Bobot_ModalUsaha` double NOT NULL,
  `Bobot_JumlahTenagaKerja` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bobot`
--

INSERT INTO `bobot` (`idBobot`, `Bobot_StatusKepimilikanTempatUMKM`, `Bobot_Omset`, `Bobot_Aset`, `Bobot_ModalUsaha`, `Bobot_JumlahTenagaKerja`) VALUES
(1, 0.2, 0.25, 0.2, 0.2, 0.15);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemilihan`
--

CREATE TABLE `pemilihan` (
  `idPemilihan` int(11) NOT NULL,
  `idPenilaian` int(11) NOT NULL,
  `NoKTP` varchar(16) NOT NULL,
  `NormalisaliStatusKepemilikanTempatUMKM` double NOT NULL,
  `NormalisasiOmset` double NOT NULL,
  `NormalisasiAset` double NOT NULL,
  `NormalisasiModalUsaha` double NOT NULL,
  `NormalisasiJumlahTenagaKerja` double NOT NULL,
  `HasilStatusKepemilikanTempatUMKM` double NOT NULL,
  `HasilOmset` double NOT NULL,
  `HasilAset` double NOT NULL,
  `HasilModalUsaha` double NOT NULL,
  `HasilJumlahTenagaKerja` double NOT NULL,
  `HasilAkhir` double NOT NULL,
  `statusRekomendasi` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengguna`
--

CREATE TABLE `pengguna` (
  `Username` varchar(15) NOT NULL,
  `Password` varchar(6) NOT NULL,
  `Nama` varchar(50) NOT NULL,
  `Level` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengguna`
--

INSERT INTO `pengguna` (`Username`, `Password`, `Nama`, `Level`) VALUES
('adi', '123456', 'adi', 'UMKM'),
('Admin', '123456', 'Arif Suryono', 'Dinas UMKM'),
('apaya', 'apaya', 'apaya', 'UMKM'),
('Bima', 'Bima', 'Bima Jaelani Anjay', 'UMKM'),
('boy', 'boy', 'boy', 'UMKM'),
('dadan', '123456', 'Dadan', 'UMKM'),
('dede', '123456', 'Dede', 'UMKM'),
('Ega Anjay', 'ega su', 'Ega Anjay Suripto', 'UMKM'),
('EKA', '123456', 'AI EKA', 'UMKM'),
('Eman', 'eman', 'Eman', 'UMKM'),
('hanafy', '123456', 'Hanafy', 'UMKM'),
('umkm', 'umkm', 'umkm', 'UMKM');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penilaian`
--

CREATE TABLE `penilaian` (
  `IdPenilaian` int(11) NOT NULL,
  `NoKTP` varchar(16) NOT NULL,
  `StatusKepemilikanTempatUMKM` double NOT NULL,
  `Omset` double NOT NULL,
  `Aset` double NOT NULL,
  `ModalUsaha` double NOT NULL,
  `JumlahTenagaKerja` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penilaian`
--

INSERT INTO `penilaian` (`IdPenilaian`, `NoKTP`, `StatusKepemilikanTempatUMKM`, `Omset`, `Aset`, `ModalUsaha`, `JumlahTenagaKerja`) VALUES
(6, '11111111', 100, 4, 8, 28, 33),
(7, '2222222222', 67, 31, 47, 34, 33),
(8, '222222', 33, 87, 67, 100, 66),
(9, '2222222', 100, 67, 67, 67, 33),
(10, '333333', 33, 67, 87, 87, 66),
(11, '44444444', 100, 87, 66, 66, 33);

-- --------------------------------------------------------

--
-- Struktur dari tabel `umkm`
--

CREATE TABLE `umkm` (
  `NoKTP` varchar(16) NOT NULL,
  `Nama_Pemilik` varchar(25) NOT NULL,
  `Nama_Usaha` varchar(20) NOT NULL,
  `Alamat` varchar(50) NOT NULL,
  `Sektor_Usaha` varchar(20) NOT NULL,
  `Wilayah` varchar(20) NOT NULL,
  `Setujui` varchar(15) NOT NULL,
  `Proposal` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `umkm`
--

INSERT INTO `umkm` (`NoKTP`, `Nama_Pemilik`, `Nama_Usaha`, `Alamat`, `Sektor_Usaha`, `Wilayah`, `Setujui`, `Proposal`) VALUES
('11111111', 'Eman', 'Eman Sulaiman', 'jl.batujajar', 'Kuliner', 'Cililin', 'Disetujui', 'Eman_Sulaeman2.pdf'),
('222222', 'umkm', 'Bakery roti', 'Jl.Batujajar', 'Kuliner', 'Cililin', 'Disetujui', 'aan_wasiah.pdf'),
('2222222', 'hanafy', 'Roti Hanafy', 'Jl.Batujajar', 'Kuliner', 'Cipeundeuy', 'Disetujui', 'Hanafy.pdf'),
('2222222222', 'EKA', 'EKA FASHION', 'jl batujajar', 'FASHION', 'Parongpong', 'Disetujui', 'AI_EKA.pdf'),
('3244412356', 'boy', 'Ruko Si Boy', 'Cisarua', 'Perdagangan', 'Cisarua', 'Belum Disetujui', 'MODUL_4_APLIKOM_20201.pdf'),
('333333', 'dede', 'Dede Fashion', 'jl cisarua', 'FASHION', 'Saguling', 'Disetujui', 'Dede.pdf'),
('44444444', 'dadan', 'Kerupuk Singkong', 'Jl Batujajar', 'Kuliner', 'Saguling', 'Disetujui', 'Dadan.pdf'),
('87678', 'umkm', 'Eman', 'Cimahi', 'Kuliner', 'Cihampelas', 'Belum Disetujui', 'LOA_SYARIFUDIN5.pdf'),
('90898', 'umkm', 'Anjay', 'Gatau', 'Kerajinan', 'Gununghalu', 'Belum Disetujui', 'LOA_SYARIFUDIN4.pdf');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `bobot`
--
ALTER TABLE `bobot`
  ADD PRIMARY KEY (`idBobot`);

--
-- Indeks untuk tabel `pemilihan`
--
ALTER TABLE `pemilihan`
  ADD PRIMARY KEY (`idPemilihan`),
  ADD KEY `pem1` (`NoKTP`);

--
-- Indeks untuk tabel `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`Username`);

--
-- Indeks untuk tabel `penilaian`
--
ALTER TABLE `penilaian`
  ADD PRIMARY KEY (`IdPenilaian`),
  ADD KEY `pen1` (`NoKTP`);

--
-- Indeks untuk tabel `umkm`
--
ALTER TABLE `umkm`
  ADD PRIMARY KEY (`NoKTP`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `pemilihan`
--
ALTER TABLE `pemilihan`
  MODIFY `idPemilihan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `penilaian`
--
ALTER TABLE `penilaian`
  MODIFY `IdPenilaian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `pemilihan`
--
ALTER TABLE `pemilihan`
  ADD CONSTRAINT `pem1` FOREIGN KEY (`NoKTP`) REFERENCES `umkm` (`NoKTP`);

--
-- Ketidakleluasaan untuk tabel `penilaian`
--
ALTER TABLE `penilaian`
  ADD CONSTRAINT `pen1` FOREIGN KEY (`NoKTP`) REFERENCES `umkm` (`NoKTP`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
