<?php
class Penilaian_model extends CI_Model{

    public function LihatPenilaian(){
      $this->db->select('*');
      $this->db->from('penilaian');
      $this->db->join('umkm','umkm.NoKTP=penilaian.NoKTP');
      $result = $this->db->get();
      return $result->result();
    }

    public function getIdPenilaian($where){
      $this->db->from('penilaian');
      $this->db->where('IdPenilaian', $where);
      $result = $this->db->get();
      return $result->result();
    }

    public function editPenilaian($table,$data,$where){
      $this->db->where('IdPenilaian', $where);
      $this->db->update($table,$data);
    }

    public function LihatBobot(){
      $this->db->select('*');
      $this->db->from('bobot');
      $result = $this->db->get();
      return $result->result();
    }

    public function Max(){
      $this->db->select_max('StatusKepemilikanTempatUMKM');
      $this->db->select_max('Omset');
      $this->db->select_max('Aset');
      $this->db->select_max('ModalUsaha');
      $this->db->select_max('JumlahTenagaKerja');
      $this->db->from('penilaian');
      $result = $this->db->get();
      return $result->result();
    }

    public function Min(){
      $this->db->select_min('StatusKepemilikanTempatUMKM');
      $this->db->select_min('Omset');
      $this->db->select_min('Aset');
      $this->db->select_min('ModalUsaha');
      $this->db->select_min('JumlahTenagaKerja');
      $this->db->from('penilaian');
      $result = $this->db->get();
      return $result->result();
    }

    public function HitungMautKeseluruhan($table,$data){
      $this->db->insert_batch($table, $data);
    }
}
?>