<?php
class Pemilihan_model extends CI_Model{
	
	public function tampilUMKM(){
		$this->db->select('*');
		$this->db->from('umkm');
		$this->db->where('Nama_Pemilik',$this->session->userdata('Username'));
	    $result = $this->db->get();
	    return $result->result();
	}

  public function tampilPemilihan(){
      $this->db->select('*');
	  $this->db->from('pemilihan');
	  $this->db->join('umkm','umkm.NoKTP=pemilihan.NoKTP');
      $this->db->order_by('HasilAkhir', 'desc');
      $result = $this->db->get();
      return $result->result();
  }

	public function tambahPendampingan($data){
    	return $this->db->insert('umkm',$data);
  	}

  	public function getNoKTP($where){
  		$this->db->from('umkm');
      $this->db->where('NoKTP', $where);
      $result = $this->db->get();
      return $result->result();
  	}

  	public function ubahPendampingan($table, $data, $where){
    	$this->db->where('NoKTP', $where);
    	$this->db->update($table,$data);
  	}

}
?>