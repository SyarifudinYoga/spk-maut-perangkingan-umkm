<?php
	class Pengguna_model extends CI_Model{
		function validasi($username,$password){
			$this->db->where('username',$username);
			$this->db->where('password',$password);
			$result = $this->db->get('pengguna',1);
			return $result;
		}

		function LihatPengguna(){
			$this->db->select('*');
    		$this->db->from('pengguna');
    		$result = $this->db->get();
    		return $result->result();
		}

		function tambahPengguna($table,$data){
			$this->db->insert($table, $data);
		}

		function getIdPengguna($where){
			$this->db->from('pengguna');
		    $this->db->where('Username', $where);
		    $result = $this->db->get();
		    return $result->result();
		}

		function editPengguna($table,$data,$where){
			$this->db->where('Username', $where);
    		$this->db->update($table,$data);
		}

		function hapusPengguna($where){
			$this->db->where('Username', $where);
    		$this->db->delete('pengguna');
		}

		function jumlahUMKM(){
			$this->db->select('*');
    		$this->db->from('umkm');
    		//$this->db->where('email =', $email);
    		return $this->db->count_all_results();
		}

		function jumlahSetujuUMKM(){
			$this->db->select('*');
    		$this->db->from('umkm');
    		$this->db->where('Setujui =',"Disetujui");
    		return $this->db->count_all_results();
		}

		function jumlahHitungUMKM(){
			$this->db->select('*');
    		$this->db->from('pemilihan');
    		//$this->db->where('email =', $email);
    		return $this->db->count_all_results();
		}

		function jumlahUser(){
			$this->db->select('*');
    		$this->db->from('pengguna');
    		//$this->db->where('email =', $email);
    		return $this->db->count_all_results();
		}

		function chartWilayah(){
			$this->db->group_by('Wilayah');
    		$this->db->select('Wilayah');
    		$this->db->select("count(*) as total");
    		return $this->db->from('umkm')->get()->result();
		}

		function chartSektorUsaha(){
			$this->db->group_by('Sektor_Usaha');
    		$this->db->select('Sektor_Usaha');
    		$this->db->select("count(*) as total");
    		return $this->db->from('umkm')->get()->result();	
		}
	}
?>