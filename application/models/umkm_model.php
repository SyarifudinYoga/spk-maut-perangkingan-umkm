<?php
	class Umkm_model extends CI_Model{

	public function tampilUMKM(){
		$this->db->select('*');
		$this->db->from('umkm');
		//$this->db->where('username =',$this->session->userdata('Username'));
	    $result = $this->db->get();
	    return $result->result();
	}

	
  	public function setujuUMKM($table,$data,$where){
  		$this->db->where('NoKTP', $where);
    	$this->db->update($table,$data);
  	}

  	//Lempar ke Tabel Penilaian
  	public function tambahPenilaianUMKM($data1){
  		return $this->db->insert('penilaian',$data1);
  	}
	
	public function getNoKTP($where){
		$this->db->from('umkm');
		$this->db->where('NoKTP', $where);
		$result = $this->db->get();
		return $result->result();
	}

	public function pilihRekomendasi($table,$data,$where){
		$this->db->where('NoKTP', $where);
    	$this->db->update($table,$data);
	}
	}
?>