<!DOCTYPE html>
<html>

<head>
    
    <title>Welcome Dinas Koperasi dan UMKM</title>

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url('plugins/bootstrap/css/bootstrap.css');?>" rel="stylesheet"> 
</head>
<body class="theme-red">
    <center><h3>Laporan Penilaian UMKM</h3></center><hr>
        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
            <thead>
                <tr>
                    <th><center>Rank</th>
                    <th><center>No KTP</th>
                    <th><center>Nama Pemilik</th>
                    <th><center>Nama Usaha</th>
                    <th><center>Alamat</th>
                    <th><center>Sektor Usaha</th>
                    <th><center>Hasil Akhir</th>
                </tr>
            </thead>
            <tbody>
            <?php 
                $no=1;
                foreach($hasil as $h){
            ?>
            <tr>
                <td><?php echo $no;?></td><?php $no++; ?>
                <td><?php echo $h->NoKTP; ?></td>
                <td><?php echo $h->Nama_Pemilik;?></td>
                <td><?php echo $h->Nama_Usaha;?></td>
                <td><?php echo $h->Alamat;?></td>
                <td><?php echo $h->Sektor_Usaha;?></td>
                <td><?php echo $h->HasilAkhir;?></td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    <script>
        window.print();
    </script>
</body>
</html>
