<?php
class Umkm extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('umkm_model');
	}
	function LihatUmkm(){
		$data['umkm'] = $this->umkm_model->tampilUMKM();
		$this->load->view('DinasUMKM/KelolaPemohon',$data);
	}

	function setujuUMKM(){
    	$NoKTP = $this->input->post('NoKTP');
		$Setujui = "Disetujui";
              $data = array(
                'NoKTP' => $NoKTP,
                'Setujui' => $Setujui
              );
              $data1 = array(
                'NoKTP' => $NoKTP
              );
              if($data == null) {
                $this->session->set_flashdata('msg',
                        '<div class="alert alert-danger">
                            <h4>Oppss</h4>
                            <p>Tidak ada data dinput.</p>
                        </div>');
                $this->LihatUMKM();
              }else{
                $this->session->set_flashdata('msg',
                        '<div class="alert alert-success alert-dismissible fade show" role="alert">
                        <p>Data <strong>UMKM </strong> Berhasil Disetujui </p>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>');
                $this->umkm_model->setujuUMKM('umkm',$data,$NoKTP);
                $this->umkm_model->tambahPenilaianUMKM($data1);
                $this->LihatUMKM();
            };
    }

    function printPemohon(){
      $NoKTP = $this->input->post('NoKTP');
      $data['umkm'] = $this->umkm_model->getNoKTP($NoKTP);
      $this->load->view('DinasUMKM/proposal', $data);
    }

    function pilihRekomendasi(){
      $NoKTP=$this->input->post('NoKTP');
      $statusRekomendasi="Rekomendasi";
      $data= array(
        'NoKTP'=>$NoKTP,
        'statusRekomendasi' => $statusRekomendasi
      );
      if($data == null) {
        $this->session->set_flashdata('msg',
                '<div class="alert alert-danger">
                    <h4>Oppss</h4>
                    <p>Tidak ada data dinput.</p>
                </div>');
        $this->LihatUMKM();
      }else{
        $this->session->set_flashdata('msg',
                '<div class="alert alert-success alert-dismissible fade show" role="alert">
                <p>Data <strong>UMKM </strong> Berhasil Direkomendasikan </p>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>');
        $this->umkm_model->pilihRekomendasi('pemilihan',$data,$NoKTP);
        $this->LihatUMKM();
    };
    }

}
?>