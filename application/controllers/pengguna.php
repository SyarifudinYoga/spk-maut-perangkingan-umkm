<?php
class Pengguna extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('pengguna_model');
	}
	function index(){
		$this->load->view('login_view');
	}
	function login(){
		$username=$this->input->post('username',TRUE);
		$password=$this->input->post('password',TRUE);
		$validasi=$this->pengguna_model->validasi($username,$password);
		if($validasi->num_rows() >0){
			$data=$validasi->row_array();
			$username=$data['Username'];
			$password=$data['Password'];
			$nama=$data['Nama'];
			$level=$data['Level'];
			$sesdata = array (
				'Username' => $username,
				'Password' => $password,
				'Nama' => $nama,
				'Level' => $level,
				'logged_in' => TRUE
			);
			$this->session->set_userdata($sesdata);

			if ($level === 'Dinas UMKM') {
				redirect('pengguna/DinasUMKM');
			} else {
				redirect('pengguna/UMKM');
			}


		} else{
			echo $this->session->set_flashdata('msg','Email atau Password Tidak Sesuai');
			redirect('pengguna');
		}
	}

	function DinasUMKM(){
		$data['chartWilayah'] = $this->pengguna_model->chartWilayah();
		$data['chartSektorUsaha'] = $this->pengguna_model->chartSektorUsaha();
		$data['jumlahUMKM'] = $this->pengguna_model->jumlahUMKM();
		$data['jumlahSetuju'] = $this->pengguna_model->jumlahSetujuUMKM();
		$data['jumlahHitung'] = $this->pengguna_model->jumlahHitungUMKM();
		$data['jumlahUser'] = $this->pengguna_model->jumlahUser();
		$this->load->view('DinasUMKM/dashboardDinasUMKM',$data);
	}

	function UMKM(){
		$this->load->view('UMKM/dashboardUMKM');
	}

	function LihatPengguna(){
		$data['pengguna'] = $this->pengguna_model->lihatPengguna();
		$this->load->view('DinasUMKM/KelolaPengguna',$data);
	}

	function formTambahPengguna(){
		$this->load->view('DinasUMKM/formTambahPengguna');	
	}

	function tambahPengguna(){
		$Username = $this->input->post('Username');
		$Password = $this->input->post('Password');
		$Nama = $this->input->post('Nama');
		$Level = $this->input->post('Level');
		$data = array (
			'Username' => $Username,
			'Password' => $Password,
			'Nama' => $Nama,
			'Level' => $Level
		);
		if($data == null) {
        $this->session->set_flashdata('tbh',
                '<div class="alert alert-danger">
                    <p>Data Pengguna gagal Diubah</p>
                </div>');
        $this->LihatPengguna();
      	}else{
        $this->session->set_flashdata('tbh',
          '<div class="alert alert-success alert-dismissible fade show" role="alert">
            <p>Data <strong>Pengguna </strong> Berhasil Ditambahkan </p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>');
        $this->pengguna_model->tambahPengguna('pengguna', $data);
        $this->LihatPengguna();
      };
	}

	function formEditPengguna(){
		$Username = $this->input->post('Username');
		$data['pengguna'] = $this->pengguna_model->getIdPengguna($Username);
		$this->load->view('DinasUMKM/formEditPengguna',$data);
	}

	function editPengguna(){
		$Username = $this->input->post('Username');
		$Password = $this->input->post('Password');
		$Nama = $this->input->post('Nama');
		$Level = $this->input->post('Level');
		$data = array (
			'Username' => $Username,
			'Password' => $Password,
			'Nama' => $Nama,
			'Level' => $Level
		);
		if($data == null) {
        $this->session->set_flashdata('edt',
                '<div class="alert alert-danger">
                    <p>Data Pengguna gagal Diubah</p>
                </div>');
        $this->LihatPengguna();
      	}else{
        $this->session->set_flashdata('edt',
          '<div class="alert alert-success alert-dismissible fade show" role="alert">
            <p>Data <strong>Pengguna </strong> Berhasil Diubah </p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>');
        $this->pengguna_model->editPengguna('pengguna', $data, $Username);
        $this->LihatPengguna();
      };	
	}

	function hapusPengguna(){
		$Username = $this->input->post('Username');
      	$this->pengguna_model->hapusPengguna($Username);
      	$this->LihatPengguna();
	}

	function registrasi(){
		$this->load->view('register_view');	
	}

	function tambahPenggunaUMKM(){
		$Username = $this->input->post('Username');
		$Password = $this->input->post('Password');
		$Nama = $this->input->post('Nama');
		$Level = "UMKM";
		$data = array (
			'Username' => $Username,
			'Password' => $Password,
			'Nama' => $Nama,
			'Level' => $Level
		);
		if($data == null) {
        $this->session->set_flashdata('tbh',
                '<div class="alert alert-danger">
                    <p>Data Pengguna gagal Diubah</p>
                </div>');
        $this->index();
      	}else{
        $this->session->set_flashdata('tbh',
          '<div class="alert alert-success alert-dismissible fade show" role="alert">
            <p>Data <strong>Pengguna </strong> Berhasil Ditambahkan </p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>');
        $this->pengguna_model->tambahPengguna('pengguna', $data);
        $this->index();
      };
	}

	function logout(){
		$this->session->sess_destroy();
		redirect('pengguna');
	}
}
?>