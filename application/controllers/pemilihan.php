<?php
class Pemilihan extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('pemilihan_model');
	}
	
	function LihatPendampingan(){
		$data['normalisasi'] = $this->pemilihan_model->tampilPemilihan();
    $data['maut'] = $this->pemilihan_model->tampilPemilihan();
    $data['hasil'] = $this->pemilihan_model->tampilPemilihan();
    $this->load->view('DinasUMKM/KelolaPendampingan',$data);
	}


  //===================================================================================umkm
	function LihatPendampinganUMKM(){
		$data['umkm'] = $this->pemilihan_model->tampilUMKM();
		$this->load->view('UMKM/KelolaPendampingan',$data);	
	}

	function formTambahPendampingan(){
		$this->load->view('UMKM/formTambahPendampingan');
	}

	function tambahPendampingan(){
		$NoKTP = $this->input->post('NoKTP');
		$Nama_Pemilik = $this->session->userdata('Username');
		$Nama_Usaha = $this->input->post('Nama_Usaha');
		$Alamat = $this->input->post('Alamat');
		$Sektor_Usaha = $this->input->post('Sektor_Usaha');
    $Wilayah = $this->input->post('Wilayah');
		$Setujui = "Belum Disetujui";
		//get Pdf
        $config['upload_path'] = './proposal';
        $config['allowed_types'] = 'pdf';
        $config['max_size'] = '2048';  //2MB max
        $config['file_name'] = $_FILES['Proposal']['name'];
        $this->upload->initialize($config);
        if (!empty($_FILES['Proposal']['name'])) {
          if ( $this->upload->do_upload('Proposal') ) {
            $file = $this->upload->data();
              $data = array(
                'NoKTP' => $NoKTP,
                'Nama_Pemilik' => $Nama_Pemilik,
                'Nama_Usaha' => $Nama_Usaha,
                'Alamat' => $Alamat,
                'Sektor_Usaha' => $Sektor_Usaha,
                'Wilayah' => $Wilayah,
                'Setujui' => $Setujui,
                'Proposal' => $file['file_name'],
              );
              if($data == null) {
                $this->session->set_flashdata('msg',
                        '<div class="alert alert-danger">
                            <h4>Oppss</h4>
                            <p>Tidak ada data dinput.</p>
                        </div>');
                $this->LihatPendampinganUMKM();
              }else{
                $this->session->set_flashdata('msg',
                        '<div class="alert alert-success alert-dismissible fade show" role="alert">
                        <p>Data <strong>UMKM </strong> Berhasil Ditambahkan </p>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>');
                $this->pemilihan_model->tambahPendampingan($data);
                $this->LihatPendampinganUMKM();
            };
          }else {
              die("gagal upload");
          }
        }else {
          echo "tidak masuk";
        }
	}

	function printPendampingan(){
      $NoKTP = $this->input->post('NoKTP');
      $data['umkm'] = $this->pemilihan_model->getNoKTP($NoKTP);
      $this->load->view('UMKM/proposal', $data);
    }

    function formEditPendampingan(){
    	$NoKTP = $this->input->post('NoKTP');
    	$data['umkm'] = $this->pemilihan_model->getNoKTP($NoKTP);
    	$this->load->view('UMKM/formEditPendampingan', $data);
    }

    function editPendampingan(){
    	$NoKTP = $this->input->post('NoKTP');
		$Nama_Pemilik = $this->session->userdata('Username');
		$Nama_Usaha = $this->input->post('Nama_Usaha');
		$Alamat = $this->input->post('Alamat');
		$Sektor_Usaha = $this->input->post('Sektor_Usaha');
		$Setujui = "Belum Disetujui";
		//get Pdf
        $config['upload_path'] = './proposal';
        $config['allowed_types'] = 'pdf';
        $config['max_size'] = '2048';  //2MB max
        $config['file_name'] = $_FILES['Proposal']['name'];
        $this->upload->initialize($config);
        if (!empty($_FILES['Proposal']['name'])) {
          if ( $this->upload->do_upload('Proposal') ) {
            $file = $this->upload->data();
              $data = array(
                'NoKTP' => $NoKTP,
                'Nama_Pemilik' => $Nama_Pemilik,
                'Nama_Usaha' => $Nama_Usaha,
                'Alamat' => $Alamat,
                'Sektor_Usaha' => $Sektor_Usaha,
                'Setujui' => $Setujui,
                'Proposal' => $file['file_name'],
              );
              if($data == null) {
                $this->session->set_flashdata('msg',
                        '<div class="alert alert-danger">
                            <h4>Oppss</h4>
                            <p>Tidak ada data dinput.</p>
                        </div>');
                $this->LihatPendampinganUMKM();
              }else{
                $this->session->set_flashdata('msg',
                        '<div class="alert alert-success alert-dismissible fade show" role="alert">
                        <p>Data <strong>UMKM </strong> Berhasil Ditambahkan </p>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>');
                $this->pemilihan_model->ubahPendampingan('umkm',$data,$NoKTP);
                $this->LihatPendampinganUMKM();
            };
          }else {
              die("gagal upload");
          }
        }else {
          echo "tidak masuk";
        }	
    }

    function LihatLaporan(){
      $data['normalisasi'] = $this->pemilihan_model->tampilPemilihan();
      $data['maut'] = $this->pemilihan_model->tampilPemilihan();
      $data['hasil'] = $this->pemilihan_model->tampilPemilihan();
      $this->load->view('DinasUMKM/KelolaLaporan',$data);
    }

    function CetakLaporan(){
      $data['hasil'] = $this->pemilihan_model->tampilPemilihan();
      $this->load->view('DinasUMKM/CetakLaporan',$data);
    }

    
}
?>