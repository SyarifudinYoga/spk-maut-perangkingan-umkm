<?php
class Penilaian extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('penilaian_model');
	}

	function LihatPenilaian(){
		$data['umkm'] = $this->penilaian_model->LihatPenilaian();
		$this->load->view('DinasUMKM/KelolaPenilaian',$data);	
	}

	function formEditPenilaian(){
		$IdPenilaian = $this->input->post('IdPenilaian');
		$data['umkm'] = $this->penilaian_model->getIdPenilaian($IdPenilaian);
		$this->load->view('DinasUMKM/formEditPenilaian',$data);		
	}

	function editPenilaian(){
		$IdPenilaian = $this->input->post('IdPenilaian');
    	$NoKTP = $this->input->post('NoKTP');
    	$StatusKepemilikanTempatUMKM = $this->input->post('StatusKepemilikanTempatUMKM');
    	$Omset = $this->input->post('Omset');
    	$Aset = $this->input->post('Aset');
    	$ModalUsaha = $this->input->post('ModalUsaha');
    	$JumlahTenagaKerja = $this->input->post('JumlahTenagaKerja');
    	$data = array (
    		'IdPenilaian' => $IdPenilaian,
    		'NoKTP' => $NoKTP,
    		'StatusKepemilikanTempatUMKM' => $StatusKepemilikanTempatUMKM,
    		'Omset' => $Omset,
    		'Aset' => $Aset,
    		'ModalUsaha' => $ModalUsaha,
    		'JumlahTenagaKerja' => $JumlahTenagaKerja
    	);
    	if($data == null) {
            $this->session->set_flashdata('msg',
                '<div class="alert alert-danger">
                    <h4>Oppss</h4>
                    <p>Tidak ada data dinput.</p>
                </div>');
            $this->LihatPenilaian();
        }else{
            $this->session->set_flashdata('msg',
                '<div class="alert alert-success alert-dismissible fade show" role="alert">
                    <p>Data <strong>Penilaian </strong> Berhasil Ditambahkan </p>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                </div>');
            $this->penilaian_model->editPenilaian('penilaian',$data,$IdPenilaian);
            $this->LihatPenilaian();
        };
	}

	function HitungMaut(){
		$IdPenilaian = $this->input->post('IdPenilaian');
    	$data['nilai'] = $this->penilaian_model->LihatPenilaian();
   		$data['IdPenilaian'] = $this->penilaian_model->getIdPenilaian($IdPenilaian);
    	$data['max'] = $this->penilaian_model->Max();
    	$data['min'] = $this->penilaian_model->Min();
    	$data['bobot'] = $this->penilaian_model->LihatBobot();
    	$this->load->view('DinasUMKM/formHitungMaut', $data);
	}

	function HitungMautKeseluruhan(){
		$IdPenilaian = $this->input->post('IdPenilaian');
    	$NoKTP = $this->input->post('NoKTP');
    	$StatusKepemilikanTempatUMKM = $this->input->post('StatusKepemilikanTempatUMKM');
    	$Omset = $this->input->post('Omset');
    	$Aset = $this->input->post('Aset');
    	$ModalUsaha = $this->input->post('ModalUsaha');
    	$JumlahTenagaKerja = $this->input->post('JumlahTenagaKerja');
    	//Max
    	$maxStatusKepemilikanTempatUMKM = $this->input->post('maxStatusKepemilikanTempatUMKM');
    	$maxOmset = $this->input->post('maxOmset');
    	$maxAset = $this->input->post('maxAset');
    	$maxModalUsaha = $this->input->post('maxModalUsaha');
    	$maxJumlahTenagaKerja = $this->input->post('maxJumlahTenagaKerja');
    	//Min
    	$minStatusKepemilikanTempatUMKM = $this->input->post('minStatusKepemilikanTempatUMKM');
    	$minOmset = $this->input->post('minOmset');
    	$minAset = $this->input->post('minAset');
    	$minModalUsaha = $this->input->post('minModalUsaha');
    	$minJumlahTenagaKerja = $this->input->post('minJumlahTenagaKerja');
    	//Bobot
    	$bobotStatusKepemilikanTempatUMKM = $this->input->post('bobotStatusKepemilikanTempatUMKM');
    	$bobotOmset = $this->input->post('bobotOmset');
    	$bobotAset = $this->input->post('bobotAset');
    	$bobotModalUsaha = $this->input->post('bobotModalUsaha');
    	$bobotJumlahTenagaKerja = $this->input->post('bobotJumlahTenagaKerja');

   		//Selisih (Max-Min)
   		$selisihStatusKepemilikanTempatUMKM = $maxStatusKepemilikanTempatUMKM-$minStatusKepemilikanTempatUMKM;
    	$selisihOmset = $maxOmset-$minOmset;
    	$selisihAset = $maxAset-$minAset;
    	$selisihModalUsaha = $maxModalUsaha-$minModalUsaha;
    	$selisihJumlahTenagaKerja = $maxJumlahTenagaKerja-$minJumlahTenagaKerja;

    	$data= array();
    	$index=0;
      	foreach($IdPenilaian as $idp){
      		//Kriteria - Min
      		$k1 = $StatusKepemilikanTempatUMKM[$index] - $minStatusKepemilikanTempatUMKM;
    		$k2 = $Omset[$index] - $minOmset;
    		$k3 = $Aset[$index] - $minAset;
    		$k4 = $ModalUsaha[$index] - $minModalUsaha;
    		$k5 = $JumlahTenagaKerja[$index] - $minJumlahTenagaKerja;
    		//Normalisasi (N=Normalisasi)
    		$N1= $k1/$selisihStatusKepemilikanTempatUMKM;
		    $N2= $k2/$selisihOmset;
		    $N3= $k3/$selisihAset;
		    $N4= $k4/$selisihModalUsaha;
		    $N5= $k5/$selisihJumlahTenagaKerja;
		    //Maut (A=Alternatif)
		    $A1 = $N1*$bobotStatusKepemilikanTempatUMKM;
		    $A2 = $N2*$bobotOmset;
		    $A3 = $N3*$bobotAset;
		    $A4 = $N4*$bobotModalUsaha;
		    $A5 = $N5*$bobotJumlahTenagaKerja;
		    $hasilHitung = $A1+$A2+$A3+$A4+$A5;

		    array_push($data,array(
	        'idPenilaian' => $idp,
	        'noKTP' => $NoKTP[$index],
	        'NormalisaliStatusKepemilikanTempatUMKM' => $N1,
	        'NormalisasiOmset' => $N2,
	        'NormalisasiAset' => $N3,
	        'NormalisasiModalUsaha' => $N4,
	        'NormalisasiJumlahTenagaKerja' => $N5,
	        'HasilStatusKepemilikanTempatUMKM' => $A1,
	        'HasilOmset' => $A2,
	        'HasilAset' => $A3,
	        'HasilModalUsaha' => $A4,
	        'HasilJumlahTenagaKerja' => $A5,
	        'HasilAkhir' => $hasilHitung
	        ));
   		$index++;
      	}

      	if($data == null) {
	    $this->session->set_flashdata('hhm',
	      '<div class="alert alert-danger">
	      <p>Data <strong>Perhitungan Penilaian UMKM</strong> Gagal Ditambahkan </p>
	      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <span aria-hidden="true">&times;</span>
	      </button>
	      </div>');
	      $this->LihatPenilaian();
		}else{
		    $this->session->set_flashdata('hhm',
		      '<div class="alert alert-success alert-dismissible fade show" role="alert">
		        <p>Data <strong>Perhitungan Penilaian Rumah</strong> Berhasil Ditambahkan </p>
		          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		            <span aria-hidden="true">&times;</span>
		          </button>
		      </div>');
		$this->penilaian_model->HitungMautKeseluruhan('pemilihan', $data);
		$this->LihatPenilaian();
		};
    
	}
    
    function hapus_penilaian(){
        $this->db->empty_table('pemilihan');
        $this->LihatPenilaian(); 
    }
}
?>